"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* The hub config file
"""
import os
from tkinter import PhotoImage

# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"

### this files should be used for the over all button style and design of the
### hub

#button icons.
###General System icons###
nc = PhotoImage(file="/usr/share/pixmaps/network-wired.png")
pr = PhotoImage(file="/usr/share/pixmaps/gnome-dev-printer.png")
du = PhotoImage(file="/usr/share/pixmaps/drive-harddisk-system.png")
ug = PhotoImage(file="/usr/share/pixmaps/stock_people.png")
pa = PhotoImage(file="/usr/share/pixmaps/stock_music-library.png")
xd = PhotoImage(file= spath + '/images/xd.png')
hbon = PhotoImage(file="/usr/share/pixmaps/hblockon.png")
hboff = PhotoImage(file="/usr/share/pixmaps/hblock.png")
si = PhotoImage(file="/usr/share/pixmaps/gnome-session.png")
um = PhotoImage(file="/usr/share/pixmaps/update-manager.png")
xf = PhotoImage(file="/usr/share/pixmaps/applications-system.png")



##label text
lnc = "Network"
lpr = "Printers"
ldu = "Disk Utilities"
lug = "User & Groups"
lpa = "Pulse Audio"
lxd = "xDaily"
lhb = "hBlock"
lsi = "Update Mgr"
lum = "System Info"
lxf = "xfce Settings"


sp = PhotoImage(file= spath + '/images/peppermint-inst-48.png')
ah = PhotoImage(file="/usr/share/pixmaps/ai.png")
sc = PhotoImage(file="/usr/share/pixmaps/synaptic.png")
ss = PhotoImage(file="/usr/share/pixmaps/snap.png")
gs = PhotoImage(file="/usr/share/pixmaps/gnome-software.png")
fp = PhotoImage(file="/usr/share/pixmaps/flat.png")


lsp = "Suggested"
lah = "App Image Hub"
lsc = "Synaptic"
lss = "Snap Store"
lssw = "Snap Web Store"
lgs = "Gnome Store"
lgsw = "Gnome Web Store"
lfp = "Flat hub"


lmf = "--The PepHub  is a central place that is used to manage the build."
" It includes tools that are not readily available in the XFCE settings"
" manager,  Some of these tools are built and maintained by the Peppermint"
" team,  others are system utilities."
