"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this files should be used for the over all style and design of
* the Welcome Screen
"""
from tkinter import PhotoImage
import ttkbootstrap as ttk
import os

# This will set the style to used for bootstrap
# just change the name to what is needed for the
# the system
bbstyle = ttk.Window(themename="darkly")

# set the title of the window
DEBIAN_TITLE = "Welcome to Peppermint - (Debian)"
DEVUAN_TITLE = "Welcome to Peppermint - (Devuan)"

# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"

# add the logo text
peppertxt = PhotoImage(file=spath + '/images/peppermint-word-white.png'
                       )



# set the icons that are used for the community buttons
tms = PhotoImage(file=spath + '/images/mn.png')
fm = PhotoImage(file=spath + '/images/sf.png')
mat = PhotoImage(file=spath + '/images/mt.png')
cb = PhotoImage(file=spath + '/images/cb.png')

# set the message for the community section
MESSAGE_TEXT = ("Come join the conversation about"
                " PeppermintOS in the community.")
