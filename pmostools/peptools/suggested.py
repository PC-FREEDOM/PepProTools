"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This is for the suggested packages GUI
"""
import os
import subprocess
import tkinter as tk
import ttkbootstrap as ttk

pwin = ttk.Window(themename="darkly")
pwin.title("Suggested Packages")
pwin.resizable(False, False)
pwin.geometry('470x470')


def check_packages():
    """ Check if software is installed"""
    if os.path.exists('/usr/bin/atril'):
        pdf_viewer['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/snap'):
        snap['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/parole'):
        media_player['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/gufw'):
        fire_wall['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/flatpak'):
        flatpak['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/timeshift'):
        backup_tool['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/gnome-software'):
        store['state'] = tk.DISABLED


def check_web_browsers():
    """ Check what browsers are installed"""
    if os.path.exists('/usr/bin/firefox'):
        fire_fox['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/konqueror'):
        kde_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/qutebrowser'):
        qute_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/falkon'):
        falkon_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/chromium'):
        chromium_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/epiphany-browser'):
        gnome_web['state'] = tk.DISABLED
    if os.path.exists('/usr/bin/gnome-software'):
        store['state'] = tk.DISABLED


# ***Install Packages***
def install_atril():
    """ Install atril """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "atril"]
                    )
    check_packages()


def install_parole():
    """ Install parole """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "parole"]
                    )
    check_packages()


def install_gufw():
    """ Install gufw """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "gufw"]
                    )
    check_packages()


def install_snap():
    """ Install install snap"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "snapd"]
                    )
    check_packages()


def install_flatpak():
    """ Install flatpak """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "flatpak"]
                    )
    check_packages()


def install_store():
    """ Install gnome store"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "gnome-software"]
                    )
    check_packages()


def install_timeshift():
    """ Install timeshift"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "timeshift"]
                    )
    check_packages()


# ***Web browser defs***
def install_firefox():
    """ Install firefox-esr """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "firefox-esr"]
                    )
    check_web_browsers()


def install_konqueror():
    """ Install konqueror """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "konqueror"]
                    )
    check_web_browsers()


def install_epiphany():
    """ Install epiphany-browser """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "epiphany-browser"]
                    )
    check_web_browsers()


def install_tor():
    """ Install torbrowser-launcher """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "torbrowser-launcher"]
                    )
    check_web_browsers()


def install_qute():
    """ Install install qutebrowser"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "qutebrowser"]
                    )
    check_web_browsers()


def install_chromium():
    """ Install chromium"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "chromium"]
                    )
    check_web_browsers()


def install_falkon():
    """ Install falkon"""
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "apt", "install",
                      "falkon"]
                    )
    check_web_browsers()


# *** Below here is the layout of the forms***
# Frame that managed the Welcome To Peppermint Section
fsw = ttk.Frame(pwin, width=200)
fsw.grid(row=2, column=0, columnspan=4)
software_frame = ttk.Labelframe(fsw, bootstyle="warning",
                                text="Suggested Software"
                                )
software_frame.grid(row=5, column=0, columnspan=2, ipadx=0, ipady=0,
                    padx=10, pady=10
                    )
# Buttons for the suggested software
pdf_viewer = ttk.Button(software_frame, text="Atril: a document viewer",
                        cursor="hand2", style="dark", command=install_atril
                        )
pdf_viewer.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                sticky='ew'
                )
media_player = ttk.Button(software_frame, text="Parole: Media Player",
                          cursor="hand2", style="dark", command=install_parole
                          )
media_player.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                  sticky='ew'
                  )
fire_wall = ttk.Button(software_frame, text="gufw: GNU Firewall",
                       cursor="hand2", style="dark", command=install_gufw
                       )
fire_wall.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5,
               sticky='ew'
               )
snap = ttk.Button(software_frame, text="Snap Package Platform",
                  cursor="hand2", style="dark", command=install_snap
                  )
snap.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5,
          sticky='ew'
          )
flatpak = ttk.Button(software_frame, text="Flatpak Package Platform",
                     cursor="hand2", style="dark", command=install_flatpak
                     )
flatpak.grid(row=5, column=0, ipadx=5, ipady=5, padx=5, pady=5,
             sticky='ew'
             )
store = ttk.Button(software_frame, text="Gnome Software Store",
                   cursor="hand2", style="dark", command=install_store
                   )
store.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5,
           sticky='ew'
           )
backup_tool = ttk.Button(software_frame, text="TimeShift: Backup Tool",
                         cursor="hand2", style="dark",
                         command=install_timeshift
                         )
backup_tool.grid(row=7, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                 sticky='ew'
                 )
# Frame that manages the Community buttons
fs = ttk.Frame(pwin)
fs.grid(row=2, column=5, columnspan=2, ipadx=0, ipady=0, padx=10, pady=10)

# Frame Title
soc = ttk.Labelframe(fs, bootstyle="warning", text="Suggested Web Browsers")
soc.grid(row=5, column=0, columnspan=2, ipadx=0, ipady=0, padx=10, pady=10)

# Web Browser Buttons
fire_fox = ttk.Button(soc, text="Firefox ESR Browser", cursor="hand2",
                      style="dark", command=install_firefox
                      )
fire_fox.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

kde_web = ttk.Button(soc, text="Konqueror KDE Browser", cursor="hand2",
                     style="dark", command=install_konqueror
                     )
kde_web.grid(row=1, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

gnome_web = ttk.Button(soc, text="Gnome Web(Epiphany)", cursor="hand2",
                       style="dark", command=install_epiphany
                       )
gnome_web.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

tor_web = ttk.Button(soc, text="Tor Browser: Privacy", cursor="hand2",
                     style="dark", command=install_tor
                     )
tor_web.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

qute_web = ttk.Button(soc, text="Qutebrowser: Vim-like ", cursor="hand2",
                      style="dark", command=install_qute
                      )
qute_web.grid(row=4, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

chromium_web = ttk.Button(soc, text="Chromium Browser", cursor="hand2",
                          style="dark-", command=install_chromium
                          )
chromium_web.grid(row=6, column=0, ipadx=5, ipady=5, padx=5, pady=5,
                  sticky='ew'
                  )
falkon_web = ttk.Button(soc, text="Falkon Qt Browser", cursor="hand2",
                        style="dark", command=install_falkon
                        )
falkon_web.grid(row=7, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

lblpmtitle = ttk.Label(pwin, style="F62817.TLabel",
                       text='These suggestions are listed in the official'
                            ' repositories'
                       ).place(x=5, y=440)
check_packages()
check_web_browsers()
pwin.mainloop()
